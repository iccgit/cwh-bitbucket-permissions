# cwh-bitbucket-permissions
Script to populate `AWS_ROLE_ARN` as Deployment environment variable in a CWH bitbucket Code repo.

## Example
```yaml
- pipe: iccgit/cwh-bitbucket-permissions:<tag>
    variables:
      BITBUCKET_BRANCH: hotfix
      BITBUCKET_ADDENV: [test|staging|production]
      ASSUME_ROLE_HOTFIX: $ASSUME_ROLE_HOTFIX
```

## Work
- It strips the `-infra` suffix to the `BITBUCKET_REPO_SLUG` env var to find the repo to configure.
- Requires `BITBUCKET_USER` and `BITBUCKET_PASSWORD` to login to Bitbucket.
- Role to assume is calculated as `"ASSUME_ROLE_" + BITBUCKET_BRANCH`

## Setup env vars

```
export AWS_ACCESS_KEY_ID="xxxx"
export AWS_SECRET_ACCESS_KEY="xxxxx"
export AWS_REGION=eu-west-1
export BITBUCKET_REPO_SLUG=icc-cwh-xxxxx-infra 
export BITBUCKET_BRANCH=dev 
export ASSUME_ROLE_DEV=arn:aws:iam::xxxxxx:role/infrastructureAsCodeRole
export AWS_REGION=eu-west-1
export BITBUCKET_USER=bb-cloud-platformops
export BITBUCKET_PASSWORD=xxxxx
```

## Use it
Once all env var are configured.

```shell
go build
./cwh-bitbucket-permissions
```

## Docker usage
```shell
docker build -t bb-permissions .
docker run --rm --env-file test.env bb-permissions
```
You don't need to use the script at the end of `docker run` as it is configured as ENTRYPOINT
