package main

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"

	"encoding/json"
)

func main() {
	projectName := strings.ReplaceAll(getRequiredEnvVar("BITBUCKET_REPO_SLUG"), "-infra", "")
	projectBranch := getRequiredEnvVar("BITBUCKET_BRANCH")
	getRequiredEnvVar("AWS_REGION")

	projectNameWithEnv := projectName + "-" + projectBranch
	if projectBranch == "master" {
		projectNameWithEnv = projectName
	}

	awsRole := getRequiredEnvVar("ASSUME_ROLE_" + strings.ToUpper(projectBranch))
	bitbucketUser := getRequiredEnvVar("BITBUCKET_USER")
	bitbucketPass := getRequiredEnvVar("BITBUCKET_PASSWORD")
	owner := getRequiredEnvVar("BITBUCKET_WORKSPACE")

	mapWithDefaultRepoEnvs := map[string]string{
		"dev":    "test",
		"test":   "staging",
		"master": "production",
	}

	mappedBranch, exists := mapWithDefaultRepoEnvs[projectBranch]

	if !exists {
		envType := os.Getenv("BITBUCKET_ADDENV")
		if envType == "test" || envType == "staging" || envType == "production" {
			fmt.Printf("New environment %v will be created in %v environments deployments\n", projectBranch, envType)
		} else {
			fmt.Printf("Environment variable BITBUCKET_ADDENV not provided/valid. Using Staging as default for %v.\n", projectBranch)
			envType = "staging"
		}
		createEnvironment(bitbucketUser, bitbucketPass, projectName, owner, projectBranch, envType)
		mappedBranch = projectBranch
	}

	fmt.Println("Getting secret " + projectNameWithEnv)
	awsPipelineRole, err := getAWSSecret(awsRole, projectNameWithEnv)

	if err != nil {
		fmt.Println("Error getting secret value from AWS secrets:" + err.Error())
		awsRole = "ERROR-FROM-CWH-PERMISSIONS"
	}

	err = updateDeploymentVars(bitbucketUser, bitbucketPass, projectName, owner, mappedBranch, "AWS_ROLE_ARN", awsPipelineRole, false)
	if err != nil {
		fmt.Printf("Error updating deploy var AWS_ROLE_ARN in %v app repo: '%v", projectName, err.Error())
		panic("Error updating deploy var AWS_ROLE_ARN: " + err.Error())
	} else {
		fmt.Printf("Updated AWS_ROLE_ARN var for %v\n", projectName)
	}
}

func getAWSSecret(role string, secretName string) (string, error) {
	var err error
	sess := session.Must(session.NewSession())
	// Create the credentials from AssumeRoleProvider to assume the role
	// referenced by the role ARN.
	creds := stscreds.NewCredentials(sess, role)

	// Create service client value configured for credentials
	// from assumed role.
	svc := secretsmanager.New(sess, &aws.Config{Credentials: creds})

	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretName),
	}

	result, err := svc.GetSecretValue(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeResourceNotFoundException:
				fmt.Println(secretsmanager.ErrCodeResourceNotFoundException, aerr.Error())
			case secretsmanager.ErrCodeInvalidParameterException:
				fmt.Println(secretsmanager.ErrCodeInvalidParameterException, aerr.Error())
			case secretsmanager.ErrCodeInvalidRequestException:
				fmt.Println(secretsmanager.ErrCodeInvalidRequestException, aerr.Error())
			case secretsmanager.ErrCodeDecryptionFailure:
				fmt.Println(secretsmanager.ErrCodeDecryptionFailure, aerr.Error())
			case secretsmanager.ErrCodeInternalServiceError:
				fmt.Println(secretsmanager.ErrCodeInternalServiceError, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return "", err
	}

	var secret map[string]interface{}
	err = json.Unmarshal([]byte(*result.SecretString), &secret)
	if err != nil {
		return "", err
	}

	// Check if the key exists in the secret map
	if val, ok := secret["aws_pipeline_role"].(string); ok {
		return val, nil
	}

	// Handle the case when the key does not exist
	return "", errors.New("key 'aws_pipeline_role' does not exist in secret")
}

func getRequiredEnvVar(varname string) string {
	read_var, exists := os.LookupEnv(varname)
	if !exists {
		fmt.Printf("Variable %v not defined\n", varname)
		panic("Variable needed")
	}
	return read_var
}
