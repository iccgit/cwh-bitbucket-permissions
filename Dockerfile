FROM golang:alpine as builder
RUN mkdir /build 
ADD . /build/
WORKDIR /build
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o cwh-bitbucket-permissions .

FROM alpine:3
RUN apk --update add git less openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

WORKDIR /app
COPY --from=builder /build/cwh-bitbucket-permissions /app
COPY config.yaml /app

COPY README.md pipe.yml /

ENTRYPOINT ["/app/cwh-bitbucket-permissions"]