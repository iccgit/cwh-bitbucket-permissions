package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	bitbucket "github.com/ktrysmt/go-bitbucket"
	"github.com/spf13/viper"
)

func createEnvironment(user string, pass string, repoName string, owner string, projectBranch string, envType string) (string, bool) {
	mapWithenvs := map[string]bitbucket.RepositoryEnvironmentTypeOption{
		"test":       0,
		"staging":    1,
		"production": 2,
	}
	envTypeOption := mapWithenvs[envType]

	envVariables, securedVariables := getConfig(envTypeOption)

	c := bitbucket.NewBasicAuth(user, pass)

	envopt := &bitbucket.RepositoryEnvironmentOptions{
		Owner:           owner,
		RepoSlug:        repoName,
		Name:            projectBranch,
		EnvironmentType: envTypeOption,
		Rank:            1,
	}

	environment, err := c.Repositories.Repository.AddEnvironment(envopt)
	if err != nil {
		fmt.Printf("Environment %v already exists. Skipping creation.\n", projectBranch)
		return "exists", true
	}

	uuid := environment.Uuid
	deployEnvironmentRepo, _ := getDeployRepo(user, pass, repoName, owner, envType)
	addvars, err := addDeploymentVars(user, pass, repoName, owner, environment, uuid, envVariables, false, deployEnvironmentRepo)
	if err != nil {
		fmt.Println("Error adding unsecured variables", addvars)
	}
	addsecvars, err := addDeploymentVars(user, pass, repoName, owner, environment, uuid, securedVariables, true, deployEnvironmentRepo)
	if err != nil {
		fmt.Println("Error adding secured variables", addsecvars)
	}
	return "created", true
}

func getConfig(environment bitbucket.RepositoryEnvironmentTypeOption) (map[string]string, map[string]string) {
	ex, _ := os.Executable()
	path := filepath.Dir(ex)
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	// fmt.Println("Config file path:", viper.ConfigFileUsed())

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config file: %s", err)
	}

	envVariables := viper.GetStringMapString(fmt.Sprintf("deployments.%s.env", environment))
	securedVariables := viper.GetStringMapString(fmt.Sprintf("deployments.%s.secured", environment))

	return envVariables, securedVariables
}

func addDeploymentVars(user string, pass string, repoName string, owner string, environment *bitbucket.Environment, uuid string, variables map[string]string, secure bool, env_repo string) (bool, error) {
	c := bitbucket.NewBasicAuth(user, pass)
	process := false
	for key, value := range variables {
		if strings.ToUpper(key) == "BITBUCKET_PASSWORD" {
			value = pass
		}
		if strings.ToUpper(key) == "DEPLOY_ENVIRONMENT_REPO" {
			value = env_repo
		}
		opt3 := &bitbucket.RepositoryDeploymentVariableOptions{
			Owner:       owner,
			RepoSlug:    repoName,
			Environment: environment,
			Uuid:        uuid,
			Key:         strings.ToUpper(key),
			Value:       value,
			Secured:     secure,
		}
		_, errvar := c.Repositories.Repository.AddDeploymentVariable(opt3)
		if errvar != nil {
			fmt.Printf("Var '%v' exists in '%v': '%v'\n", strings.ToUpper(key), environment.Name, errvar)
		} else {
			process = true
		}

	}

	return process, nil
}

func getDeployRepo(user string, pass string, repoName string, owner string, envType string) (string, error) {
	fmt.Printf("Getting DEPLOY_ENVIRONMENT_REPO from %v environment\n", envType)
	c := bitbucket.NewBasicAuth(user, pass)
	listenv := &bitbucket.RepositoryEnvironmentsOptions{
		Owner:    owner,
		RepoSlug: repoName,
	}
	environments, err := c.Repositories.Repository.ListEnvironments(listenv)
	if err != nil {
		return "", err
	}

	environment, err := findEnvironmentByName(envType, environments)
	if err != nil {
		return "", err
	}
	listvar := &bitbucket.RepositoryDeploymentVariablesOptions{
		Owner:       owner,
		RepoSlug:    repoName,
		Environment: environment,
	}

	env_repo, err := c.Repositories.Repository.ListDeploymentVariables(listvar)
	for _, var_repo := range env_repo.Variables {
		if var_repo.Key == "DEPLOY_ENVIRONMENT_REPO" {
			return var_repo.Value, nil
		}
	}
	return "", err
}
