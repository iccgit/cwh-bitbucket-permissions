module cwh-bitbucket-permissions

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.59
	github.com/ktrysmt/go-bitbucket v0.9.15
	github.com/spf13/viper v1.18.2 // indirect
)
