package main

import (
	"fmt"
	"strings"

	bitbucket "github.com/ktrysmt/go-bitbucket"
)

func updateDeploymentVars(user string, pass string, repoName string, owner string, projectBranch string, deploymentVarKey string, deploymentVarValue string, secured bool) error {
	c := bitbucket.NewBasicAuth(user, pass)

	environment, err := getEnvironment(c, owner, repoName, projectBranch)
	if err != nil {
		return fmt.Errorf("failed to get environment: %w", err)
	}
	deployVarUUID, err := getDeployVariablesUUID(c, owner, repoName, environment, deploymentVarKey)
	if err != nil {
		return err
	}

	deployVariable := &bitbucket.RepositoryDeploymentVariableOptions{
		Owner:       owner,
		RepoSlug:    repoName,
		Environment: environment,
		Key:         deploymentVarKey,
		Value:       deploymentVarValue,
		Uuid:        deployVarUUID,
		Secured:     secured,
	}

	_, err = c.Repositories.Repository.UpdateDeploymentVariable(deployVariable)
	if err != nil {
		return err
	}

	return nil
}

func findEnvironmentByName(name string, e *bitbucket.Environments) (*bitbucket.Environment, error) {
	for _, environment := range e.Environments {
		if strings.EqualFold(environment.Name, name) {
			return &environment, nil
		}
	}

	return nil, fmt.Errorf("no environment named %s found in %d available environments", name, len(e.Environments))
}

func getDeployVariablesUUID(c *bitbucket.Client, owner string, repoName string, environment *bitbucket.Environment, key string) (string, error) {

	listOpt := &bitbucket.RepositoryDeploymentVariablesOptions{
		Owner:       owner,
		RepoSlug:    repoName,
		Environment: environment,
		Pagelen:     45,
	}

	deploymentVariables, err := c.Repositories.Repository.ListDeploymentVariables(listOpt)
	if err != nil {
		return "", err
	}

	for _, deploymentVariable := range deploymentVariables.Variables {
		if deploymentVariable.Key == key {
			return deploymentVariable.Uuid, nil
		}

	}
	return "", fmt.Errorf("variable not found in list")
}

func getEnvironment(bbc *bitbucket.Client, owner string, repoName string, envName string) (*bitbucket.Environment, error) {
	opt := &bitbucket.RepositoryEnvironmentsOptions{
		Owner:    owner,
		RepoSlug: repoName,
	}

	environments, _ := bbc.Repositories.Repository.ListEnvironments(opt)
	environment, err := findEnvironmentByName(envName, environments)
	if err != nil {
		return nil, fmt.Errorf("environment not found: %w", err)
	}

	return environment, nil
}
